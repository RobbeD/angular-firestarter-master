import * as functions from 'firebase-functions';
import * as geofirex from 'geofirex'
import * as firebase from 'firebase';

// const transactionals = require('./transactionals');
const firex = geofirex.init(firebase)
const config = {
    apiKey: "AIzaSyDEs6ZS4gmHjLFtzhQg2J609cu675vsuBA",
    authDomain: "pinguintest-c5cba.firebaseapp.com",
    databaseURL: "https://pinguintest-c5cba.firebaseio.com",
    projectId: "pinguintest-c5cba",
    storageBucket: "pinguintest-c5cba.appspot.com",
    messagingSenderId: "13697258096",
    sendgrid: {
        key: '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',
        template: 'd-c19ba2aadea1475d83f99dd763301c95'
    }
}
firebase.initializeApp(config);
const db = firebase.firestore();


// export const sendMail = functions.https.onRequest((request, response) => {
//     transactionals.yolo(request, response);
// });

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// // });
// export const getArtists = functions.https.onRequest((request, response) => {

export const onDocument = functions.firestore.document('artists/{artistid}').onCreate(() => {
    db.collection('artists').orderBy(`geo.lat` && `geo.lng`).get().then(res => {
        res.forEach(doc => {
            console.log(doc.id)
            const gArt = doc.data()
            const center = firex.point(gArt.geo.lat, gArt.geo.lng)
            console.log({ oldGeo: gArt.geo, newGeo: center })
            const data = {
                geo: {
                    lat: firebase.firestore.FieldValue.delete(),
                    lng: firebase.firestore.FieldValue.delete(),
                    geohash: center.geohash,
                    geopoint: center.geopoint
                }
            }
            console.log(data)
            db.doc(`artists/${doc.id}`).set(data, { merge: true })
        })

    })
})