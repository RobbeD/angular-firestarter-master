export const environment = {
  production: true,
  shell: {
    debug: false,
    networkDelay: 2000
  },
  firebaseConfig: {
    apiKey: "AIzaSyDEs6ZS4gmHjLFtzhQg2J609cu675vsuBA",
    authDomain: "pinguintest-c5cba.firebaseapp.com",
    databaseURL: "https://pinguintest-c5cba.firebaseio.com",
    projectId: "pinguintest-c5cba",
    storageBucket: "pinguintest-c5cba.appspot.com",
    messagingSenderId: "13697258096"
  },
};