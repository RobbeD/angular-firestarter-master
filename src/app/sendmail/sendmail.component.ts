import { Component, OnInit } from '@angular/core';
import { PinguingramService } from '../services/pinguingram.service';
import { AngularFireFunctions } from '@angular/fire/functions';
import { AngularFireAuth } from '@angular/fire/auth';
import { take, switchMap, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { combineLatest, of } from 'rxjs';

@Component({
  selector: 'app-sendmail',
  templateUrl: './sendmail.component.html',
  styleUrls: ['./sendmail.component.scss']
})
export class SendmailComponent implements OnInit {
  // form fields
  field: string = '';
  value: string = '';
  text: string = '';
  subject: string = '';
  selectedValue: any;
  selectedCol: any;
  selectedTrueFalse: any;
  selectedOperator: any;
  trueorfalse: any;
  templateId: any;
  // fill comboboxes
  operatorList: Array<Object> = [{ name: "==" }, { name: "array-contains" }, { name: "<" }, { name: "<=" }, { name: ">=" }, { name: ">" }, { name: "" },];
  collections: Array<Object> = [{ name: "users" }, { name: "artists" }];

  // public function variables
  collectionData: any;
  artist: any;
  doclength: any;
  adminDoc: any;
  emailArray: any;
  emailData: any;
  observableEmailData: any;

  constructor(
    private afs: AngularFirestore,
    private pservice: PinguingramService,
    public afAuth: AngularFireAuth,
    public fun: AngularFireFunctions
  ) { }

  ngOnInit() {
    this.selectedOperator = '';
    this.selectedTrueFalse = '';
    this.templateId = '';
  }

  sendSingles() {
    if (this.selectedCol == 'users') {
      this.emailData.to.forEach(element => {
        const emailDataFinal = {
          to: element.email,
          TEMPLATE_ID: this.templateId,
          dynamicData: {
            subject: this.emailData.subject,
            text: this.emailData.text,
          }
        }
        const callable = this.fun.httpsCallable('genericEmail');
        callable(emailDataFinal).subscribe();
      })
    } else if (this.selectedCol == 'artists') {
      this.emailData.to.forEach(element => {
        const emailDataFinal = {
          to: element.email,
          TEMPLATE_ID: this.templateId,
          dynamicData: {
            subject: this.emailData.subject,
            text: this.emailData.text,
            artistname: element.name
          }
        }
        const callable = this.fun.httpsCallable('genericEmail');
        callable(emailDataFinal).subscribe();
      })
    }
  }
  sendBulk() {
    if (this.selectedCol == 'users') {
      const emails = []
      this.emailData.to.forEach(element => {
        emails.push(element.email)
      })
      const finalEmail = Array.from(new Set(emails))
      const emailDataFinal = {
        to: finalEmail,
        TEMPLATE_ID: this.templateId,
        dynamicData: {
          subject: this.emailData.subject,
          text: this.emailData.text,
        }
      }
      const callable = this.fun.httpsCallable('genericEmail');
      callable(emailDataFinal).subscribe();
    } else if (this.selectedCol == 'artists') {
      const emails = []
      this.emailData.to.forEach(element => {
        emails.push(element.email)
      })
      const finalEmail = Array.from(new Set(emails))
      const emailDataFinal = {
        to: finalEmail,
        TEMPLATE_ID: this.templateId,
        dynamicData: {
          subject: this.emailData.subject,
          text: this.emailData.text,
        }
      }
      const callable = this.fun.httpsCallable('genericEmail');
      callable(emailDataFinal).subscribe();
    }
  }

  sendPersonal(item) {
    this.emailData = {
      to: item.email,
      TEMPLATE_ID: this.templateId,
      dynamicData: {
        subject: this.emailData.subject,
        text: this.emailData.text,
        artistname: item.name
      }
    }
    const callable = this.fun.httpsCallable('genericEmail');
    callable(this.emailData).subscribe();
  }

  sendmails(f) {
    // form values
    const field = f.value.field
    const value = f.value.value
    this.subject = f.value.subject
    this.text = f.value.text
    this.selectedCol = f.value.selectedValue.name
    const operator = f.value.selectedOperator.name
    this.selectedTrueFalse = f.value.trueorfalse
    this.templateId = f.value.templateId
    // arrays
    this.emailArray = [];
    this.observableEmailData = [];
    if (this.selectedCol == 'artists') {
      if (field != '' || value != '') {
        this.afs.collection(this.selectedCol, (ref) => ref.where(field, operator, value || this.selectedTrueFalse).where('claimed', '==', true)).valueChanges({ idField: 'id' })
          .pipe(
            map(res => {
              this.artist = res;
              this.doclength = res.length;
              this.artist.map(resp => {
                const adm = resp.admin;
                if (adm != null) {
                  let collectionData = Object.keys(adm) as any[];
                  const adminArray = [];
                  collectionData.map(key => {
                    adminArray.push(`users/${key}`);
                  })
                  adminArray.forEach(key => {
                    this.pservice.document$(key).subscribe(respo => {
                      this.adminDoc = respo
                      this.emailArray.push({
                        email: this.adminDoc.email,
                        name: resp.name
                      })
                    })
                  });
                  this.emailData = {
                    to: this.emailArray,
                    subject: this.subject,
                    text: this.text
                  }
                } else {
                  console.log('no admin')
                }
              });
            })
          ).subscribe()
      } else if (field == '' || value == '') {
        this.afs.collection(this.selectedCol, (ref) => ref.orderBy('admin')).valueChanges({ idField: 'id' })
          .pipe(
            map(res => {
              this.artist = res;
              this.doclength = res.length;
              this.artist.map(resp => {
                const adm = resp.admin;
                if (adm != null) {
                  let collectionData = Object.keys(adm) as any[];
                  const adminArray = [];
                  collectionData.map(key => {
                    adminArray.push(`users/${key}`);
                  })
                  adminArray.map(docKey => {
                    this.pservice.document$(docKey).subscribe(res => {
                      this.adminDoc = res
                      this.emailArray.push({
                        email: this.adminDoc.email,
                        name: resp.name
                      })
                    })
                  });
                  this.emailData = {
                    to: this.emailArray,
                    subject: this.subject,
                    text: this.text
                  }

                } else {
                  console.log('no admin')
                }
              });
            })
          ).subscribe()
      }
    } else if (this.selectedCol == 'users') {
      if (field != '' || value != '') {
        this.afs.collection(this.selectedCol, (ref) => ref.where(field, operator, value || this.selectedTrueFalse)).valueChanges({ idField: 'id' }).pipe(take(1)).subscribe(res => {
          this.collectionData = res
          this.doclength = res.length;
          this.collectionData.map(data => {
            this.emailArray.push({
              email: data.email,
              name: data.username
            })
          });
          this.emailData = {
            to: this.emailArray,
            subject: this.subject,
            text: this.text
          }
          this.observableEmailData.push(this.emailData)

        })
      } else if (field == '' || value == '') {
        this.pservice.collection$(this.selectedCol).pipe(take(1)).subscribe(res => {
          this.collectionData = res
          this.doclength = res.length;
          this.collectionData.map(data => {
            this.emailArray.push({
              email: data.email,
              name: data.username
            })
          });
          this.emailData = {
            to: this.emailArray,
            subject: this.subject,
            text: this.text
          }
          this.observableEmailData.push(this.emailData)
        })
      }
    }
  }
}
