import { Component, OnInit } from '@angular/core';
import { PinguingramService } from '../services/pinguingram.service';
import { leftJoin } from '../services/join.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { take, switchMap, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { firestore } from 'firebase/app';
import { combineLatest, of } from 'rxjs';
import * as geofirex from 'geofirex'
import * as firebase from 'firebase/app'

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  uid: any;
  user: any;
  artist: any;
  joined$: any;
  posts: any;
  userID: any;
  id: any;
  claimed: boolean;
  oldGeo: any;
  geoArtist: any;
  geofirex = geofirex.init(firebase)

  constructor(
    private pservice: PinguingramService,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) { }


  ngOnInit(): void {
    this.afAuth.authState.subscribe(res => {
      if (res) {
        this.uid = res.uid
        // this.sendDataToCloudServices();
        // this.toGeoHash();
      }
    })
  }


  async joinCollections() {
    this.pservice.collection$('users').subscribe(res => {
      this.user = res;
      this.user.forEach(element => {
        this.id = element.id
        this.afs.collection('artists', ref => ref.where(`admin.${this.id}`, '==', true)).valueChanges({ idField: 'id' }).pipe(take(1)).subscribe(res => {
          if (res.length > 0) {
            this.artist = res;
            for (let index = 0; index < this.artist.length; index++) {
              const element = this.artist[index];
              console.log(element)
              let data = {
                artist: [{
                  artist_id: element.id,
                  artist_name: element.name,
                  recieve_emails: true,
                  role: 'admin'
                }]
              }
              console.log(data)
            }
          }
        })
      });
    })
  }

  updateDocument() {
    this.afs.collection('artists', (ref) => ref.orderBy('admin')).valueChanges({ idField: 'id' })
      .pipe(
        switchMap(res => {
          this.artist = res;
          if (this.artist.claimed === false) {
            this.claimed = false;
            return [];
          } else {

            this.artist.map(res => {
              const adm = res.admin;
              let collectionData = Object.keys(adm) as any[];
              const adminArray = [];

              for (let i = 0; i < collectionData.length; i++) {
                const key = collectionData[i];
                adminArray.push(`users/${key}`);
              }

              adminArray.forEach(key => {
                const data = {
                  artist: firestore.FieldValue.arrayUnion({
                    artist_id: res.id,
                    artist_name: res.name,
                    recieve_emails: true,
                    role: 'admin'
                  })
                }
                this.pservice.documentUpdate$(`${key}`, data)
              });

              return adminArray.length ? combineLatest(adminArray) : of([]);
            });
          }
        })
      ).subscribe(res => console.log(res))
  }

  // sendDataToCloudServices() {
  //   this.pservice.collection$(`users`).subscribe(res => {
  //     this.user = res;
  //     this.user.map(res => {
  //       if (res.artist) {
  //         res.artist.forEach(element => {
  //           const data = {
  //             id: res.id,
  //             artist: element
  //           }
  //           console.log(data)
  //         });
  //       }
  //     })
  //   })
  // }

  toGeoHash() {
    this.afs.collection('artists', ref => ref.orderBy(`geo.lat` && `geo.lng`)).valueChanges({ idField: 'id' }).subscribe(res => {
      this.geoArtist = res
      this.geoArtist.map(gArt => {
        console.log(gArt)
        const center = this.geofirex.point(gArt.geo.lat, gArt.geo.lng)
        console.log({ oldGeo: gArt.geo, newGeo: center })
        const data = {
          geo: {
            lat: firestore.FieldValue.delete(),
            lng: firestore.FieldValue.delete(),
            geohash: center.geohash,
            geopoint: center.geopoint
          }
        }
        this.pservice.documentUpdate$(`artists/${gArt.id}`, data)
      })

    })
  }

}

