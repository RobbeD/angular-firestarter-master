import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PinguingramService {
  dataCollection: AngularFirestoreCollection<any>

  constructor(private afs: AngularFirestore) { }


  document$(path: string) {
    return this.afs.doc(path).valueChanges()
  }
  getCollection$Where(path, type?, query?) {
    return this.afs
      .collection(path, (ref) => ref.where(type, `==`, query))
      .get()
  }

  get$(path: string) {
    return this.afs.doc(path)
  }

  collection$(path: string) {
    return this.afs.collection(path).valueChanges({ idField: 'id' })

  }

  collectionOnce(path, type?, query?) {
    return this.afs
      .collection(path, (ref) => ref.where(type, `==`, query))
      .snapshotChanges()
      .pipe(
        take(1),
        map(actions => {
          return actions.map(a => {
            const data: Object = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  collectionWhere$(path: string, type: string, query: any): Observable<any[]> {
    this.dataCollection = this.afs.collection(path, (ref) => ref.where(type, `==`, query));
    return this.dataCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data();
          return { id: a.payload.doc.id, ...data };
        });
      })
    );
  }

  collectionArrayContains$(path: string, type: string, query: any): Observable<any[]> {
    this.dataCollection = this.afs.collection(path, (ref) => ref.where(type, `array-contains`, query));
    return this.dataCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data();
          return { id: a.payload.doc.id, ...data };
        });
      })
    );
  }


  collectionWhereOrder$(path: string, type: string, query: any, orderBy: any): Observable<any[]> {
    this.dataCollection = this.afs.collection(path, (ref) => ref.where(type, `==`, query).orderBy(orderBy, 'desc'));
    return this.dataCollection.snapshotChanges().pipe(
      map((actions) => {
        return actions.map((a) => {
          const data = a.payload.doc.data();
          return { id: a.payload.doc.id, ...data };
        });
      })
    );
  }


  documentUpdate$(path: string, item: any) {
    return this.afs.doc<any>(path).set(item, { merge: true })
  }
  documentUpdateMergeFalse$(path: string, item: any) {
    return this.afs.doc<any>(path).set(item, { merge: false })
  }
  documentAdd$(path: string, item: any) {
    return this.afs.collection<any>(path).add(item)
  }

  documentDelete$(path: string) {
    return this.afs.doc<any>(path).delete()
  }


  public exists(path: string): Promise<boolean> {
    return new Promise(resolve => {
      this.afs.doc(path).valueChanges().pipe(take(1)).subscribe(res => {
        if (res) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  }
}
